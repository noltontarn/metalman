package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameOverScreen extends ScreenAdapter{

	private MetalManGame metalmangame;
	private World world;
	private Texture gameoverImg;
	private Texture gameclearImg;
	private SpriteBatch batch;
	
	public GameOverScreen(MetalManGame metalmangame, World world) {
		this.metalmangame = metalmangame;
		this.world = world;
		batch = metalmangame.batch;
		gameoverImg = new Texture("gameover.jpg");
		gameclearImg = new Texture("gameclear.jpg");
	}
	
	public void render(float delta) {
		update(delta);
		batch.begin();
		if(world.bossdead) {
			batch.draw(gameclearImg, 0, 0);
		}
		else {
			batch.draw(gameoverImg, 0, 0);
		}
		batch.end();
	}
	
	public void update(float delta) {
		if(Gdx.input.isKeyPressed(Keys.ENTER)) {
			metalmangame.setScreen(new GameScreen(metalmangame));
		}
	}
} 