package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

public class Boss {
	private Vector2 position;
	public int Face = FACERIGHT;
	public static final int FACERIGHT = 1;
	public static final int FACELEFT = 2;
	public static final int SPEED = 4;
	public int HP = 600;
	private int countbullet = 0;
	private boolean checkdelay = false;
	private boolean shootbullet = false;
	private long bullettimer;
	private ArrayList<EnemyBullet> enemybullet;
	
	public Boss(int x, int y, int face, ArrayList<EnemyBullet> enemybullet) {
		this.enemybullet = enemybullet;
		position = new Vector2(x,y);
		setFace(face);
	}
	
    public void setFace(int face) {
        Face = face;
    }
  
    public void getDamage(int damage) {
        HP -= damage;
        if(HP <= 0) {
      	  HP = 0;
        }
        System.out.println(HP);
    }
  
    public Vector2 getPosition() {
        return position;    
    }
  
    public void update() {
    	delayEnemyBullet();
	    if(Face == FACERIGHT) {
	    	if(shootbullet != true) {
	    		position.x += SPEED;
	    	}
		    if(getPosition().x > 650) {
			    setFace(FACELEFT);
			    shootbullet = true;
		    }
			if(TimeUtils.millis() - bullettimer >100 && checkdelay == true && shootbullet == true) {
				enemybullet.add(new EnemyBullet(getPosition().x, getPosition().y + 55, Face, false, true));
				countbullet++;
				checkdelay = false;
				if(countbullet == 3) {
					shootbullet = false;
					countbullet = 0;
				}
	   	   	}
	    }
	    else if(Face == FACELEFT) {
	    	if(shootbullet != true) {
	    		position.x -= SPEED;
	    	}
		    if(getPosition().x < -50) {
			    setFace(FACERIGHT);
			    shootbullet = true;
		    }
			if(TimeUtils.millis() - bullettimer >100 && checkdelay == true && shootbullet == true) {
				enemybullet.add(new EnemyBullet(getPosition().x, getPosition().y + 55, Face, false, true));
				countbullet++;
				checkdelay = false;
				if(countbullet == 3) {
					shootbullet = false;
					countbullet = 0;
				}
	   	   	}
	    }
    }
    
	private void delayEnemyBullet() {
		if(checkdelay == false) {
			bullettimer = TimeUtils.millis();
			checkdelay = true;
		}
	}
}